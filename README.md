# README #

This README documents whatever steps are necessary for RNA-seq analysis in Chen et al., *Neuorn* (2021)

## RNA-seq analysis pipeline ##

### Raw reads processing ###

* Adapter trimming：trimmomatic
* FastQC after trimming
* Filtering out rRNA reads: bowtie2
* Aligning reads: STAR
* Counts data: htseq

### Differential Accessible Region (DRA) analysis ###

* edgeR
* RUVseq

### Contact Information ###

* Lead contact: Dr. Joseph Dougherty, jdougherty@wust.edu