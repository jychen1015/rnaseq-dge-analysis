## Set Working Directory
setwd("A:/Dougherty Lab/Myt1L/RNAseq/E14 cortex/results")
## Load required library
library("edgeR")
library("biomaRt")
library("ggplot2")
library("data.table")
library("combinat")
library("gridExtra")
library("gplots")
library("plyr")
library("Rtsne")
library("stringr")
library("ggforce")
library("readr")
library("dplyr")
library("ggpubr")
library("tidyverse")
library("VennDiagram")
library("Hmisc")
library("corrplot")
library("reshape2")
library("ggrepel")
library("DESeq2")
library("RUVSeq")

## Read HTSeq counts and generate dge object
files <- dir(path = "htseq_count/", pattern = "*htseq.txt$")
counts <- readDGE(files, path = "results/htseq_count")$counts
# filter out metadata fields in counts 
noint <- rownames(counts) %in% c("__no_feature","__ambiguous","__too_low_aQual",
                                "__not_aligned","__alignment_not_unique")
counts <- counts[!noint,]
if (!file.exists("DGE analysis/With RUVr/objs/genes_97.rds")){
# get gene information from ensembl
ensembl <- useEnsembl(biomart = "ENSEMBL_MART_ENSEMBL", version = 97, dataset = "mmusculus_gene_ensembl")
stopifnot(length(unique(rownames(counts))) == nrow(counts))
gtf.ens <- getBM(attributes=c('ensembl_gene_id','external_gene_name',"gene_biotype"),
                 filters = 'ensembl_gene_id', values = rownames(counts), mart = ensembl)
gtf.ens <- as.data.table(gtf.ens)
saveRDS(gtf.ens, "DGE analysis/With RUVr/objs/genes_97.rds")
}
genes <- readRDS("DGE analysis/With RUVr/objs/genes_97.rds")
genes <- genes[match(rownames(counts), genes$ensembl_gene_id),]
stopifnot(genes$ensembl_gene_id == rownames(counts))
#I created a .csv file containing all sample information
samples<-read.csv("DGE analysis/With RUVr/samples.csv")
colnames(counts)<-samples$sample
dge <- DGEList(counts = counts, 
               group = samples$group, 
               genes = genes
)
dge$samples$group <- samples$group
dge$samples$replicate <- samples$replicate
# store only protein coding genes for later analysis
dge <- dge[dge$genes$gene_biotype=="protein_coding",]
saveRDS(dge, "DGE analysis/With RUVr/objs/dge_protein_coding.rds")
# filter out lowly expressed genes
dge <- readRDS("DGE analysis/With RUVr/objs/dge_protein_coding.rds")
keep<-filterByExpr(dge)
table(keep)
dge <- dge[keep, , keep.lib.sizes=FALSE]
dge <- calcNormFactors(dge, method = "upperquartile")
saveRDS(dge, "DGE analysis/With RUVr/objs/dge_filt_norm.rds")

## Read dge object and generate log2(cpm) table
dge <- readRDS("DGE analysis/With RUVr/objs/dge_filt_norm.rds")
# get the log2(cpm) 
cpm <- data.table(dge$genes, cpm(dge, log = T))
cpm <- cpm[,-3,with=FALSE]
write.csv(cpm, "DGE analysis/With RUVr/reports/cpm.csv", row.names = F)

## Selected all samples for differential analysis
dge <- readRDS("DGE analysis/With RUVr/objs/dge_filt_norm.rds")

### Find DEG by edgeR
## Design the matrix by genotype
genotype <- factor(dge$samples$group, levels = c("WT","Het","Hom"))
design<- model.matrix(~genotype)
## Explore data: BCV plot (Biological coefficient of variation)
dge<- estimateDisp(dge, design)
## Check the common BCV value
message("common dispersion = ", sqrt(dge$common.dispersion))
pdf("DGE analysis/With RUVr/figs/Before_RUV_BCV.pdf")
plotBCV(dge)
dev.off()
## Remove unwanted variation using Residuals (RUVseq)
fit <- glmFit(dge, design)
res <- residuals(fit, type="deviance")
countsUQ<-betweenLaneNormalization(dge$counts, which="upper")
controls<-rownames(dge$counts)
counts_RUVr<-RUVr(countsUQ, controls, k=2, res)
counts_after_RUV<-counts_RUVr$normalizedCounts

## Get DGEs before RUVr normalization
fit_de_Het <- glmLRT(fit, coef = 2)
fit_de_Hom <- glmLRT(fit, coef = 3)
de_Het<-as.data.frame(topTags(fit_de_Het, n=Inf, sort.by = "PValue"))
de_Hom<-as.data.frame(topTags(fit_de_Hom, n=Inf, sort.by = "PValue"))
write.csv(de_Het, "DGE analysis/With RUVr/reports/DE_Het-WT_before_RUVr.csv")
write.csv(de_Hom, "DGE analysis/With RUVr/reports/DE_Hom-WT_before_RUVr.csv")

## Get DGEs after RUVr normalization
genes <- readRDS("DGE analysis/With RUVr/objs/genes_97.rds")
genes <- genes[match(rownames(counts_after_RUV), genes$ensembl_gene_id),]
dge_RUVr <- DGEList(counts = counts_after_RUV, 
                    group = samples$group,
                    genes = genes
)
dge_RUVr$samples$replicate <- samples$replicate
dge_RUVr$samples$sex<-samples$sex
dge_RUVr$samples$group <- samples$group
# store only protein coding genes for later analysis
dge_RUVr <- dge_RUVr[dge_RUVr$genes$gene_biotype=="protein_coding",]
saveRDS(dge_RUVr, "DGE analysis/With RUVr/objs/dge_RUVr_protein_coding.rds")
# normalizes for RNA composition
dge_RUVr <- calcNormFactors(dge_RUVr, method = "upperquartile")
saveRDS(dge_RUVr, "DGE analysis/With RUVr/objs/dge_RUVr_filt_norm.rds")
# get the log2(cpm) 
dge_RUVr <- readRDS("DGE analysis/With RUVr/objs/dge_RUVr_filt_norm.rds")
cpm_after_RUVr<- data.table(dge_RUVr$genes, cpm(dge_RUVr, log = T))
cpm_after_RUVr <- cpm_after_RUVr[,-3,with=FALSE]
write.csv(cpm_after_RUVr, "DGE analysis/With RUVr/reports/cpm_after_RUVr.csv", row.names = F)
dge_RUVr$samples$group <- factor(dge_RUVr$samples$group, levels = c("WT","Het","Hom"))
# Explore data: BCV plot (Biological coefficient of variation)
dge_RUVr<- estimateDisp(dge_RUVr, design)
# Check the common BCV value
message("common dispersion = ", sqrt(dge_RUVr$common.dispersion))
pdf("DGE analysis/With RUVr/figs/After_RUV_BCV.pdf")
plotBCV(dge_RUVr)
dev.off()
#Find DGEs by glmFit
fit_RUVr <- glmFit(dge_RUVr, design)
fit_de_RUVr_Het <- glmLRT(fit_RUVr, coef = 2)
fit_de_RUVr_Hom <- glmLRT(fit_RUVr, coef = 3)
fit_de_RUVr_geno<- glmLRT(fit_RUVr, coef = 2:3)
fit_de_RUVr_Hom_Het<-glmLRT(fit_RUVr, contrast = c(0,-1,1))
de_RUVr_Het<-as.data.frame(topTags(fit_de_RUVr_Het, n=Inf, sort.by = "PValue"))
de_RUVr_Hom<-as.data.frame(topTags(fit_de_RUVr_Hom, n=Inf, sort.by = "PValue"))
de_RUVr_geno<-as.data.frame(topTags(fit_de_RUVr_geno, n=Inf, sort.by = "PValue"))
de_RUVr_Hom_Het<-as.data.frame(topTags(fit_de_RUVr_Hom_Het, n=Inf, sort.by = "PValue"))
write.csv(de_RUVr_Het, "DGE analysis/With RUVr/reports/DE_Het-WT_after_RUVr.csv")
write.csv(de_RUVr_Hom, "DGE analysis/With RUVr/reports/DE_Hom-WT_after_RUVr.csv")
write.csv(de_RUVr_geno, "DGE analysis/With RUVr/reports/DE_geno_after_RUVr.csv")
write.csv(de_RUVr_Hom_Het, "DGE analysis/With RUVr/reports/DE_Hom-Het_after_RUVr.csv")
