#!/bin/bash 
#SBATCH --mem=50G 
#SBATCH -J align_rrna 
#SBATCH --array=1-18
#SBATCH -o /working directory/logs/align_rrna.out 
#SBATCH -e /working directory/logs/align_rrna.err 

module load bowtie2 
ID=$( sed -n ${SLURM_ARRAY_TASK_ID}p /working directory/lookup.txt ) 
bowtie2 --un-conc-gz /working directory/results/post_rrna_filter/${ID}_no_rRNA.fastq.gz \
        -x /working directory/genome/rrna_index/GRCm38.rrna -1 /working directory/results/trimmed/${ID}_trimmed_1P.fastq.gz -2 /working directory/results/trimmed/${ID}_trimmed_2P.fastq.gz 
        -S /working directory/results/post_rrna_filter/${ID}.sam